class ArticlesController < ApplicationController
  http_basic_authenticate_with name: "dicafi", password: "E4w0F5e7!", except: [:index, :show]

  def new
    @article = Article.new
  end

  def create
    @article = Article.new(article_params)
 
    if @article.save
      redirect_to @article
    else
      render 'new'
    end
  end

  def show
    @article = Article.find(params[:id])
  end

  def index
    @articles = Article.all
  end

  def edit
    @article = Article.find(params[:id])
  end

  def update
    @article = Article.find(params[:id])

    if dif_tit = @article[:title] != params[:title] and @article[:text] != params[:title]
      upd_ok = @article.update(article_params)
    elsif dif_tit
      upd_ok = @article.update(article_title)
    else
      upd_ok = @article.update(article_text)
    end

    if upd_ok
      redirect_to @article
    else
      render 'edit'
    end
  end

  def destroy
    @article = Article.find(params[:id])
    @article.destroy
   
    redirect_to articles_path
  end

  private
    def article_params
      params.require(:article).permit(:title, :text)
    end

    def article_title
      params.require(:article).permit(:title)
    end

    def article_text
      params.require(:article).permit(:text)
    end
end
